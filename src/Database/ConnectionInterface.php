<?php declare(strict_types=1);

namespace Database;

interface ConnectionInterface
{
    /** @return self */
    public function select();

    /** @return self */
    public function from();

    /** @return self */
    public function where();

    /** @return ResultInterface */
    public function execute();
}
