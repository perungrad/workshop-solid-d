<?php declare(strict_types=1);

namespace Security;

use Database\ConnectionInterface;
use Exceptions\InvalidCredentialsException;

class Authentication
{
    /** @var string */
    private $salt;

    /** @var ConnectionInterface */
    private $connection;

    public function __construct(string $salt, ConnectionInterface $connection) {
        $this->salt       = $salt;
        $this->connection = $connection;
    }

    /**
     * @throws InvalidCredentialsException
     */
    public function checkCredentials(string $username, string $password): boolean {
        // get user
        $query = $this->connection->select('id, password, salt')
            ->from('users')
            ->where('username = %s', $username);

        $result = $query->execute();

        $user = $result->fetch();

        if ($user === null) {
            throw new InvalidCredentialsException('User not found');
        }

        // validate password
        if ($user['password'] === crypt($password, $this->salt . $user['salt'])) {
            return true;
        }

        return false;
    }
}
